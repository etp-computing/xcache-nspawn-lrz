#!/bin/bash
set -e

if [[ $# -lt 1 ]];then
    echo "Usage: build_image.sh <img_path> [<prefix>]"
    exit 1
fi

img_path="$1"
prefix=${2:-"/"}

dnf -y --installroot "$img_path" --releasever=8 install system-release bash yum

rsync -av "${prefix}var/local/mkosi/mkosi.extra/" "$img_path"

cp "${prefix}var/local/mkosi/mkosi.postinst" "$img_path"

systemd-nspawn -D "$img_path" bash /mkosi.postinst
