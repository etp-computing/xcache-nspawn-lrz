# Files/Directories

- [/etc/systemd/nspawn/xcache.nspawn](etc/systemd/nspawn/xcache.nspawn): configuration for lauching containers from the xcache image, e.g. bind mounts
- [/etc/pki/rpm-gpg](etc/pki/rpm-gpg), [/etc/yum.repos.d](etc/yum.repos.d): yum repositories and gpg keys on the host system - used by dnf to build new images
- [/var/local/mkosi](var/local/mkosi): build dir to make new images with mkosi
  - [mkosi.default](var/local/mkosi/mkosi.default): image definition file (distribution, packages)
  - [mkosi.extra](var/local/mkosi/mkosi.extra): extra files/directories to be copied into the image (repositories and gpg keys for extra packages not in the default repositories)
  - [mkosi.postinst](var/local/mkosi/mkosi.postinst): extra commands that are executed inside the container at the end (install extra packages not in the default repositories)
- [/usr/local/bin/build_image.sh](usr/local/bin/build_image.sh): script to build a new image without mkosi

**Further information on the [wiki](https://gitlab.physik.uni-muenchen.de/etp-computing/xcache-nspawn-lrz/-/wikis/home)**
