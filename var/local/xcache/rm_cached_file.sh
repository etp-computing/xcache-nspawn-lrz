#!/bin/bash
#
# remove cache entries for file pattern
#
if [ $# -lt 1 ]; then
  echo "usage: $0 <file-pattern>"
  exit 1
fi

fpat='*'$1'*'

# check cache
xrdpath=/srv/xcache/namespace/atlas/

flist=`find $xrdpath -xtype f  -path "$fpat" `
# check RAM disk
fl2=`find /dev/shm/atlas -xtype f -path "$fpat" `

flist="$flist $fl2"

nw=`echo $flist | wc -w `
if [ $nw -lt 1 ]; then
        echo "No files found"
        exit 1
fi

flink=""

for ding in $flist; do
        if [ -L $ding ] && [ -e $ding ]; then
                f=`readlink $ding`
                flink="$flink $f"
        fi
done


echo "Removing these files "
echo $flist $flink | tr " " "\n"

echo "Ok ? (y/n)"

read ok

if [ "$ok" == "y" ]; then
        rm $flist $flink
fi
