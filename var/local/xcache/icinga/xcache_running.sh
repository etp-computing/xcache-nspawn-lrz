#!/bin/bash

if systemctl is-active systemd-nspawn@xcache --quiet
then
    echo "OK: xrootd service running"
else
    echo "CRITICAL: xrootd service not active"
    exit 2
fi

exit 0
