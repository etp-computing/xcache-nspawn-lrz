#!/bin/bash

xrdpid=$(pgrep xrootd)
nopen=$(lsof -p $xrdpid | wc -l)

if [[ $nopen -gt 10000 ]]
then
    echo "WARNING: Currently $nopen open files"
    exit 1
else
    echo "OK: No suspiciously high number of open files"
fi

exit 0
