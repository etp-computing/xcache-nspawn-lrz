#!/bin/bash

lastbeat=$(journalctl -eu xcache-vp-heartbeat.service  | grep OK | awk '{print $1" "$2" "$3}' | tail -n 1)
lastbeat=$(date --date="$lastbeat" "+%s")
now=$(date "+%s")

if [[ $((now - lastbeat)) -gt 300 ]]
then
    echo "CRITICAL: last heartbeat to VP service more than 5 minutes ago"
    exit 2
else
    echo "OK: heartbeats running fine"
fi

exit 0
